﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PasswordGenerator
{
    public partial class EditWords : Form
    {
        public EditWords()
        {
            InitializeComponent();
        }


        private void EditWords_Load(object sender, EventArgs e)
        {
            // populate the textbox with the current data
            string lines = System.IO.File.ReadAllText("..\\nouns.txt");
            richTextBox_words.Text = lines.ToString();

            progressBar.Visible = false;

        }

        private void button_clear_Click(object sender, EventArgs e)
        {
            // TO:DO warn the user they are about to clear the table, offer to save

            richTextBox_words.Text = "";
        }

        private void button_default_Click(object sender, EventArgs e)
        {
            // populate the textbox with the default data
            string lines = System.IO.File.ReadAllText("..\\default.txt");
            richTextBox_words.Text = lines.ToString();
        }

        private void button_save_Click(object sender, EventArgs e)
        {
            // ensure users don't double click
            button_save.Enabled = false;

            // start the progress bar
            progressBar.Visible = true;

            // find out how long it will take so we can display progress feedback to the user
            progressBar.Maximum = richTextBox_words.Lines.Count();

            // textbox must be almost empty, make the maximum 10
            if (richTextBox_words.Lines.Count() < 3)
            {
                progressBar.Maximum = 10;
            }

            //update the progress label
            label_status.Text = "Saving...";

            // ensure text is in lowcase
            richTextBox_words.Text = richTextBox_words.Text.ToLower();

            // ensure the user has specified atleast ONE word of every letter.
            //create an array of chars with every letter in lowercase
            char[] alphabet = "abcdefghijklmnopqrstuvwxyz".ToCharArray();


            // keep track of what loop were in
            int c = 0;
            int numLines = richTextBox_words.Lines.Count();

            // show process for a quarter at a time
            int quarterOfLines = numLines / 4;
            progressBar.Step = quarterOfLines;

            // get all the words in a list
            var words = new List<string>();
            using (var reader = new System.IO.StringReader(richTextBox_words.Text))
            {
                for (string line = reader.ReadLine(); line != null; line = reader.ReadLine())
                {
                    c++;

                    // is the line whitespace?

                    // if yes, remove it

                    // do not add line


                    // show progres and refresh once every quarter of lines in processed
                    if (c % (numLines / quarterOfLines) == 0 && c != 0)
                    {
                        progressBar.PerformStep();
                        progressBar.Refresh();
                    }

                    words.Add(line);
                }
            }


            // create an empty arraylist of all missing letters
            ArrayList lettersMissing = new ArrayList();

            // check which ones are missing
            for (int i = 0; i < alphabet.Length; i++)
            {

                // get a letter
                String letter = alphabet[i].ToString();
                
                // check if the array contains this letter
                try
                {
                    var wordsBeginningWithLetter = words.Where(n => n.StartsWith(letter));
                    if (wordsBeginningWithLetter.First() == null)
                    {
                        lettersMissing.Add(letter);
                    }
                }
                catch
                {
                    lettersMissing.Add(letter);
                }
            }



            // check if we have any missing

            if (lettersMissing.Count > 0)
            {

                String allLettersMissing = "";

                foreach (String letter in lettersMissing){
                    allLettersMissing = " " + allLettersMissing + letter;
                }

                allLettersMissing = allLettersMissing + ".";

                // there are letters missing, display them to the user - do not proceed

                if (lettersMissing.Count == 1)
                {
                    // do not use plural phrasing
                    System.Windows.Forms.MessageBox.Show("You do not have any words that begin with the letter" + allLettersMissing);
                    
                }
                else
                {
                    // use plural phrasing
                    System.Windows.Forms.MessageBox.Show("You do not have any words that begin with the letters" + allLettersMissing);

                }

                label_status.Text = "";

            }
            else
            {

                // display are you sure to user
                var result = MessageBox.Show("Are you sure?", "Are you sure you want to save changes?",
                             MessageBoxButtons.YesNo,
                             MessageBoxIcon.Question);

                // If the yes button was pressed
                if (result == DialogResult.Yes)
                {
                    // save changes
                    System.IO.File.WriteAllText("..\\nouns.txt", richTextBox_words.Text);
                    // update progress label
                    label_status.Text = "Saved";
                }
                else
                {
                    // update progress label
                    label_status.Text = "";
                }


            }

            // stop the progressbar

            progressBar.Visible = false;
            button_save.Enabled = true;

        }

        // ensure all values are empty while closing
        void closingWindows()
        {
            label_status.Text = "";
        }

    }
}
