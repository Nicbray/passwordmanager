﻿namespace PasswordGenerator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label_firstName = new System.Windows.Forms.Label();
            this.label_lastName = new System.Windows.Forms.Label();
            this.label_algorithm = new System.Windows.Forms.Label();
            this.textBox_lastName = new System.Windows.Forms.TextBox();
            this.textBox_firstName = new System.Windows.Forms.TextBox();
            this.label_password = new System.Windows.Forms.Label();
            this.textBox_password = new System.Windows.Forms.TextBox();
            this.button_generate = new System.Windows.Forms.Button();
            this.numericUpDown_algorithm = new System.Windows.Forms.NumericUpDown();
            this.label_warningFirstName = new System.Windows.Forms.Label();
            this.label_warningLastName = new System.Windows.Forms.Label();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wordsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_algorithm)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.aboutToolStripMenuItem,
            this.importToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(503, 33);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(50, 29);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            this.clearToolStripMenuItem.Size = new System.Drawing.Size(123, 30);
            this.clearToolStripMenuItem.Text = "Clear";
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(74, 29);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // label_firstName
            // 
            this.label_firstName.AutoSize = true;
            this.label_firstName.Location = new System.Drawing.Point(57, 72);
            this.label_firstName.Name = "label_firstName";
            this.label_firstName.Size = new System.Drawing.Size(90, 20);
            this.label_firstName.TabIndex = 1;
            this.label_firstName.Text = "First Name:";
            // 
            // label_lastName
            // 
            this.label_lastName.AutoSize = true;
            this.label_lastName.Location = new System.Drawing.Point(57, 143);
            this.label_lastName.Name = "label_lastName";
            this.label_lastName.Size = new System.Drawing.Size(90, 20);
            this.label_lastName.TabIndex = 2;
            this.label_lastName.Text = "Last Name:";
            // 
            // label_algorithm
            // 
            this.label_algorithm.AutoSize = true;
            this.label_algorithm.Location = new System.Drawing.Point(57, 212);
            this.label_algorithm.Name = "label_algorithm";
            this.label_algorithm.Size = new System.Drawing.Size(80, 20);
            this.label_algorithm.TabIndex = 3;
            this.label_algorithm.Text = "Algorithm:";
            // 
            // textBox_lastName
            // 
            this.textBox_lastName.Location = new System.Drawing.Point(198, 136);
            this.textBox_lastName.Name = "textBox_lastName";
            this.textBox_lastName.Size = new System.Drawing.Size(240, 26);
            this.textBox_lastName.TabIndex = 5;
            // 
            // textBox_firstName
            // 
            this.textBox_firstName.Location = new System.Drawing.Point(198, 72);
            this.textBox_firstName.Name = "textBox_firstName";
            this.textBox_firstName.Size = new System.Drawing.Size(240, 26);
            this.textBox_firstName.TabIndex = 5;
            // 
            // label_password
            // 
            this.label_password.AutoSize = true;
            this.label_password.Location = new System.Drawing.Point(57, 323);
            this.label_password.Name = "label_password";
            this.label_password.Size = new System.Drawing.Size(82, 20);
            this.label_password.TabIndex = 3;
            this.label_password.Text = "Password:";
            // 
            // textBox_password
            // 
            this.textBox_password.Location = new System.Drawing.Point(198, 320);
            this.textBox_password.Name = "textBox_password";
            this.textBox_password.Size = new System.Drawing.Size(240, 26);
            this.textBox_password.TabIndex = 5;
            // 
            // button_generate
            // 
            this.button_generate.Location = new System.Drawing.Point(61, 259);
            this.button_generate.Name = "button_generate";
            this.button_generate.Size = new System.Drawing.Size(377, 35);
            this.button_generate.TabIndex = 6;
            this.button_generate.Text = "Generate Password";
            this.button_generate.UseVisualStyleBackColor = true;
            this.button_generate.Click += new System.EventHandler(this.button_generate_Click);
            // 
            // numericUpDown_algorithm
            // 
            this.numericUpDown_algorithm.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown_algorithm.Location = new System.Drawing.Point(198, 206);
            this.numericUpDown_algorithm.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDown_algorithm.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDown_algorithm.Name = "numericUpDown_algorithm";
            this.numericUpDown_algorithm.ReadOnly = true;
            this.numericUpDown_algorithm.Size = new System.Drawing.Size(240, 26);
            this.numericUpDown_algorithm.TabIndex = 7;
            this.numericUpDown_algorithm.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label_warningFirstName
            // 
            this.label_warningFirstName.AutoSize = true;
            this.label_warningFirstName.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label_warningFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label_warningFirstName.ForeColor = System.Drawing.Color.Red;
            this.label_warningFirstName.Location = new System.Drawing.Point(217, 107);
            this.label_warningFirstName.Name = "label_warningFirstName";
            this.label_warningFirstName.Size = new System.Drawing.Size(172, 17);
            this.label_warningFirstName.TabIndex = 8;
            this.label_warningFirstName.Text = "Please enter a First Name";
            this.label_warningFirstName.Visible = false;
            // 
            // label_warningLastName
            // 
            this.label_warningLastName.AutoSize = true;
            this.label_warningLastName.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label_warningLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label_warningLastName.ForeColor = System.Drawing.Color.Red;
            this.label_warningLastName.Location = new System.Drawing.Point(217, 177);
            this.label_warningLastName.Name = "label_warningLastName";
            this.label_warningLastName.Size = new System.Drawing.Size(172, 17);
            this.label_warningLastName.TabIndex = 8;
            this.label_warningLastName.Text = "Please enter a Last Name";
            this.label_warningLastName.Visible = false;
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.wordsToolStripMenuItem});
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(79, 29);
            this.importToolStripMenuItem.Text = "Import";
            // 
            // wordsToolStripMenuItem
            // 
            this.wordsToolStripMenuItem.Name = "wordsToolStripMenuItem";
            this.wordsToolStripMenuItem.Size = new System.Drawing.Size(152, 30);
            this.wordsToolStripMenuItem.Text = "Words";
            this.wordsToolStripMenuItem.Click += new System.EventHandler(this.wordsToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(503, 385);
            this.Controls.Add(this.label_warningLastName);
            this.Controls.Add(this.label_warningFirstName);
            this.Controls.Add(this.numericUpDown_algorithm);
            this.Controls.Add(this.button_generate);
            this.Controls.Add(this.textBox_firstName);
            this.Controls.Add(this.textBox_password);
            this.Controls.Add(this.textBox_lastName);
            this.Controls.Add(this.label_password);
            this.Controls.Add(this.label_algorithm);
            this.Controls.Add(this.label_lastName);
            this.Controls.Add(this.label_firstName);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Password Generator";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_algorithm)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Label label_firstName;
        private System.Windows.Forms.Label label_lastName;
        private System.Windows.Forms.Label label_algorithm;
        private System.Windows.Forms.TextBox textBox_lastName;
        private System.Windows.Forms.TextBox textBox_firstName;
        private System.Windows.Forms.Label label_password;
        private System.Windows.Forms.TextBox textBox_password;
        private System.Windows.Forms.Button button_generate;
        private System.Windows.Forms.NumericUpDown numericUpDown_algorithm;
        private System.Windows.Forms.Label label_warningFirstName;
        private System.Windows.Forms.Label label_warningLastName;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wordsToolStripMenuItem;
    }
}

