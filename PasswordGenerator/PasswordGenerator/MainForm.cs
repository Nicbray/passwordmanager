﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PasswordGenerator
{
    public partial class MainForm : Form
    {


        public MainForm()
        {
            InitializeComponent(); 
        }       


        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        /*A method which allows you to input a string, it will take the first letter,
         * convert it to a capital if it is not already one, and then get you the letter
         * that is *number* away from it. Once 90 is hit (Z), it will go back to 65 (A)
         * For example, W is 87 on ASCII shart, and B is 66.
         * If we were to input the letter W, and then number 5, we would end up at 66 and
         * return the letter B. 
         */
        private String GetAscii(String s, int number)
        {
            // get only first letter and ensure it is in uppercase
            String startingLetter = s.Substring(0, 1).ToUpper();

            // convert to ASCII
            ASCIIEncoding asciiEncoder = new ASCIIEncoding();
            Byte[] bytes = asciiEncoder.GetBytes(startingLetter);

            // get int from ASCII byte encoding
            int ascii = bytes[0] + number;

            // Ensure it stays within A-Z
            if (ascii > 90)
            {
                int difference = ascii - 90;
                ascii = 64 + difference;
            }

            // place the encoded ascii value back in the array for procesing
            bytes[0] = (byte) ascii;

            // convert back to string
            string finishingLetter = asciiEncoder.GetString(bytes);

            return finishingLetter;
        }

        /*
         *A method which generates a password which is made up of a word, a symbol and another word. 
         */
        private void DictionaryAlgorithm(String firstName, String lastName)
        {
            // get the number we are using for the enconding algorithm, 5 is default, 10 and 15 are also avaiable

            int algorithm = (int) numericUpDown_algorithm.Value;
 
            // gets corresponding ASCII letters based from this

            String first = GetAscii(firstName, algorithm);
            String last = GetAscii(lastName, algorithm);

            // convert to lowercase in order to match the case in the text file
            first = first.ToLower();
            last = last.ToLower();

            // get all the words that begin with the first and last letter
            String[] nouns = System.IO.File.ReadAllLines("..\\nouns.txt");
            nouns = nouns.ToArray();
             var firstNames = nouns.Where(n => n.StartsWith(first));
             var lastNames = nouns.Where(n => n.StartsWith(last));


            // get one at random depending on the number algorithm chosen and number of letters in first and last name
             try
             {
                 first = firstNames.ElementAt(textBox_firstName.TextLength * algorithm);
             }
             catch
             {
                 // catch and assign default if randomisation fails for any reason
                 first = firstNames.ElementAt(0);
             }
             try
             {
                 last = lastNames.ElementAt(textBox_lastName.TextLength * algorithm);
             }
             catch
             {
                 // catch and assign default if randomisation fails for any reason
                 last = lastNames.ElementAt(0);
             }
      


            // assigns one random symbol depending on the length of the first name
            String symbol;

            try
            {
                String[] symbols = { "!", "@", "#", "$", "%", "^", "&", "*", "(", "<", ">", "{", "}" };
                symbol = symbols[firstName.Length];
            }
            catch
            {
                // first name is unordinarily long, assign an @
                symbol = "@";
            }

            // password for firstname + lastname is noun+symbol+noun

            textBox_password.Text = first + symbol + last;
        }

        private void button_generate_Click(object sender, EventArgs e)
        {
            if (!badFields())
            {
                DictionaryAlgorithm(textBox_firstName.Text, textBox_lastName.Text);
            }
        }

        // Returns true if there are empty or bad fields, false if there is not
        private Boolean badFields()
        {
            if (textBox_firstName.TextLength < 2 || hasSpecialChar(textBox_firstName.Text))
            {
                // first name too short, warn user
                label_warningFirstName.Visible = true;
                return true;
            }
            else if (textBox_lastName.TextLength < 2 || hasSpecialChar(textBox_lastName.Text))
            {
                // first name must be good, retreive warning
                label_warningFirstName.Visible = false;

                // warn about last name
                label_warningLastName.Visible = true;

                return true;
            }
            else
            {
                // both must be good, retreive warnings and move on
                label_warningFirstName.Visible = false;
                label_warningLastName.Visible = false;
                return false;
            }
        }

        // checks for special characters in text
        public bool hasSpecialChar(string name)
        {
            string specialChar = @"\|!#$%&/()=?»«@£§€{}.-;'<>_,[]~";
            foreach (var item in specialChar)
            {
                if (name.Contains(item)) return true;
            }

            return false;
        }

        // resets everything to default
        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBox_firstName.Text = "";
            textBox_lastName.Text = "";
            textBox_password.Text = "";
            numericUpDown_algorithm.Value = 5;
        }

        // Shows a simple popup, with some "About" information
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.MessageBox.Show("Developed by an I.T professional who wanted a safe way of retrieving passwords, the Password Management system allows sys admins to generate passwords for users that’s are completely unique to the users first and last name. This allows system administrators to keep track of all user’s passwords without having to physically record them." + "\r\n \r\n" + "There are 3 different algorithm encoding options (5,10 and 15), so sys admins can administer new, unique passwords if one needs to be assigned." + "\r\n \r\n" +  "nicbraybr@gmail.com for any questions");
        }


    }
}
