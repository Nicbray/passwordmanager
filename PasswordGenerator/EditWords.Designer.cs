﻿namespace PasswordGenerator
{
    partial class EditWords
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox_words = new System.Windows.Forms.RichTextBox();
            this.button_clear = new System.Windows.Forms.Button();
            this.button_default = new System.Windows.Forms.Button();
            this.button_save = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.label_status = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // richTextBox_words
            // 
            this.richTextBox_words.Location = new System.Drawing.Point(12, 93);
            this.richTextBox_words.Name = "richTextBox_words";
            this.richTextBox_words.Size = new System.Drawing.Size(732, 440);
            this.richTextBox_words.TabIndex = 0;
            this.richTextBox_words.Text = "";
            // 
            // button_clear
            // 
            this.button_clear.Location = new System.Drawing.Point(12, 542);
            this.button_clear.Name = "button_clear";
            this.button_clear.Size = new System.Drawing.Size(122, 37);
            this.button_clear.TabIndex = 1;
            this.button_clear.Text = "Clear";
            this.button_clear.UseVisualStyleBackColor = true;
            this.button_clear.Click += new System.EventHandler(this.button_clear_Click);
            // 
            // button_default
            // 
            this.button_default.Location = new System.Drawing.Point(323, 542);
            this.button_default.Name = "button_default";
            this.button_default.Size = new System.Drawing.Size(122, 37);
            this.button_default.TabIndex = 1;
            this.button_default.Text = "Default";
            this.button_default.UseVisualStyleBackColor = true;
            this.button_default.Click += new System.EventHandler(this.button_default_Click);
            // 
            // button_save
            // 
            this.button_save.Location = new System.Drawing.Point(622, 542);
            this.button_save.Name = "button_save";
            this.button_save.Size = new System.Drawing.Size(122, 39);
            this.button_save.TabIndex = 2;
            this.button_save.Text = "Save";
            this.button_save.UseVisualStyleBackColor = true;
            this.button_save.Click += new System.EventHandler(this.button_save_Click);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(12, 595);
            this.progressBar.MarqueeAnimationSpeed = 30;
            this.progressBar.Maximum = 500;
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(732, 32);
            this.progressBar.Step = 2000;
            this.progressBar.TabIndex = 3;
            this.progressBar.UseWaitCursor = true;
            // 
            // label_status
            // 
            this.label_status.AutoSize = true;
            this.label_status.BackColor = System.Drawing.Color.Transparent;
            this.label_status.Location = new System.Drawing.Point(618, 601);
            this.label_status.Name = "label_status";
            this.label_status.Size = new System.Drawing.Size(0, 20);
            this.label_status.TabIndex = 4;
            this.label_status.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EditWords
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(756, 639);
            this.Controls.Add(this.label_status);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.button_save);
            this.Controls.Add(this.button_default);
            this.Controls.Add(this.button_clear);
            this.Controls.Add(this.richTextBox_words);
            this.Name = "EditWords";
            this.Text = "EditWords";
            this.Load += new System.EventHandler(this.EditWords_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox_words;
        private System.Windows.Forms.Button button_clear;
        private System.Windows.Forms.Button button_default;
        private System.Windows.Forms.Button button_save;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label label_status;
    }
}